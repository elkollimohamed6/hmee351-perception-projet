# Perception-projet

Ce repository contient le matériel nécessaire à réaliser le Projet de l'UE Perception pour la robotique.
Le projet sera effectué sur le robot mobile e-puck, il sera donc nécessaire de récuperer aussi le projet epuck-client:

[https://gitlab.com/umrob/epuck-client](https://gitlab.com/umrob/epuck-client)

Notamment songez à lire le mode d'emploi du robot (fichier instructions.docx dans epuck-client)

Le projet est constitué de quatre parties:

1) Etalonnage
2) Suivi de cible
3) Localisation par filtre de Kalman
4) Asservissement visuel

Bon travail!
